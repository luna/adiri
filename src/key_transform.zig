const std = @import("std");

const db = @import("database.zig");
const aesecb = @import("crypto/aes_ecb.zig");

const c = @import("crypto/aes_cbc.zig").c;

const TransformSeed = @enumToInt(db.HeaderField.TransformSeed);
const TransformRounds = @enumToInt(db.HeaderField.TransformRounds);

pub fn transform_key(self: *db.Database, composite_key: []const u8) ![]const u8 {
    var transform_seed = self.dyn_header.get(TransformSeed).?.TransformSeed;
    var transform_rounds = self.dyn_header.get(TransformRounds).?.TransformRounds;

    std.debug.warn("transform seed: {x} {}, rounds: {}\n", .{
        transform_seed,
        transform_seed.len,
        transform_rounds,
    });

    // algorithm: aes
    // mode: ecb
    // key = transform_seed
    // IV = all zeros, ECB does not use IVs

    // this is kinda bad but oh well
    var transformed_key: []u8 = try self.allocator.alloc(u8, 32);
    std.mem.copy(u8, transformed_key, composite_key);

    // perform one round of AES (for now)

    // do a copy of transformed_key since its both the input and
    // output of this function
    var i: i64 = 0;
    var in = try self.allocator.alloc(u8, 32);
    std.mem.copy(u8, in, transformed_key);

    var out = try self.allocator.alloc(u8, 32);

    std.debug.assert(transform_seed.len == 32);
    var root_key: [32]u8 = transform_seed[0..32].*;

    var ctx: c.mbedtls_aes_context = undefined;
    c.mbedtls_aes_init(&ctx);
    const setkey_res = c.mbedtls_aes_setkey_enc(&ctx, &root_key, 256);
    if (setkey_res != 0) {
        aesecb.mBedPrintErr(setkey_res);
        return error.MBedSetkeyFail;
    }

    const ts_start = std.time.nanoTimestamp();

    while (i < transform_rounds) : (i += 1) {
        try aesecb.encrypt(&ctx, in[0..16], out[0..16]);
        try aesecb.encrypt(&ctx, in[16..in.len], out[16..out.len]);

        if (0 <= i and i <= 2) {
            std.debug.warn("round {}: in {x} {} out {x} {}\n", .{
                i,
                in,
                in.len,
                out,
                out.len,
            });
        }

        std.mem.copy(u8, in, out);
    }
    const ts_end = std.time.nanoTimestamp();
    const delta_ts_nano = ts_end - ts_start;
    std.debug.warn("Ran {} key transform rounds, took {:.2}ms\n", .{ transform_rounds, @divFloor(delta_ts_nano, std.time.ns_per_ms) });

    return out;
}
