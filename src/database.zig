const std = @import("std");

const creds = @import("credentials.zig");
const trkey = @import("key_transform.zig");
const aescbc = @import("crypto/aes_cbc.zig");
const map = @import("map.zig");
const Entry = @import("reader.zig").Entry;
const salsa = @import("crypto/salsa20.zig");
const zlib = @import("zlib.zig");

const Allocator = std.mem.Allocator;
const BufferType = std.io.FixedBufferStream([]const u8);
const InStream = std.io.InStream(std.fs.File, std.os.ReadError, std.fs.File.read);

pub const DBDeserializer = std.io.Deserializer(
    .Little,
    .Byte,
    InStream,
);

pub const BlockDeserializer = std.io.Deserializer(
    .Little,
    .Byte,
    BufferType.InStream,
);

pub const AttrMap = std.StringHashMap([]u8);
pub const DynHeaderMap = std.AutoHashMap(u8, HeaderValue);
pub const DatabaseError = error{
    SignatureFail,
    InvalidHeader,
    InvalidCredentials,
    InvalidVersion,
};

pub const HeaderField = enum(u8) {
    End = 0,
    Comment,
    CipherID,
    CompressionFlags,
    MasterSeed,
    TransformSeed,
    TransformRounds,
    EncryptionIV,
    ProtectedStreamKey,
    StreamStartBytes,
    InnerRandomStreamID,
};

pub const HeaderValue = union(HeaderField) {
    End: void,
    Comment: []u8,
    CipherID: []u8,
    CompressionFlags: i32,
    MasterSeed: []u8,
    TransformSeed: []u8,
    TransformRounds: i64,
    EncryptionIV: []u8,
    ProtectedStreamKey: []u8,
    StreamStartBytes: []u8,
    InnerRandomStreamID: i32,
};

const KEEPASS_PRIMARY_ID = [_]u8{ 0x03, 0xd9, 0xa2, 0x9a };
const KeePassVersion = enum {
    One,
    TwoAlpha,
    Two,
};
const VER_1_ID = [_]u8{ 0xb5, 0x4b, 0xfb, 0x65 };
const VER_2_ALPHA_ID = [_]u8{ 0xb5, 0x4b, 0xfb, 0x66 };
const VER_2_ID = [_]u8{ 0xb5, 0x4b, 0xfb, 0x67 };
const VER_3_1_ID = [_]u8{ 0x67, 0xfb, 0x4b, 0xb5 };
const MAIN_CIPHER_ID = [_]u8{
    0x31,
    0xc1,
    0xf2,
    0xe6,
    0xbf,
    0x71,
    0x43,
    0x50,
    0xbe,
    0x58,
    0x05,
    0x21,
    0x6a,
    0xfc,
    0x5a,
    0xff,
};

fn deserializeSlice(
    allocator: *std.mem.Allocator,
    deserializer: anytype,
    comptime T: type,
    size: usize,
) ![]T {
    var value = try allocator.alloc(T, size);

    var i: usize = 0;
    while (i < size) : (i += 1) {
        value[i] = try deserializer.deserialize(T);
    }

    return value;
}

/// Unpad given data.
fn unpad(data: []u8) []u8 {
    std.debug.warn("{} {}\n", .{ data.len, data[data.len - 1] });
    var lim = data.len - data[data.len - 1];
    return data[0..lim];
}

pub const Database = struct {
    allocator: *Allocator,
    deserializer: DBDeserializer,
    attrs: AttrMap,
    dyn_header: DynHeaderMap,

    closed: bool,

    decrypted_data: []u8,
    //cipher_mappings: []u8,
    data: []u8,
    version: [2]i16,

    salsa_counter: usize = 0,
    string_arena: std.heap.ArenaAllocator,

    /// Initialize the Database with a given stream and an allocator.
    /// The stream must be an InStream, and prefferably from a file opened
    /// in read mode.
    pub fn init(stream: *InStream, allocator: *Allocator) !*Database {
        var db = try allocator.create(Database);

        db.allocator = allocator;
        db.deserializer = DBDeserializer.init(stream.*);
        db.attrs = AttrMap.init(allocator);
        db.dyn_header = DynHeaderMap.init(allocator);
        db.string_arena = std.heap.ArenaAllocator.init(allocator);
        db.salsa_counter = 0;

        db.closed = false;
        db.decrypted_data = undefined;
        db.data = undefined;
        db.version = undefined;

        return db;
    }

    pub fn deinit(self: *Database) void {
        self.string_arena.deinit();
    }

    /// Consume a variable length string from the stream.
    fn consumeString(self: *Database, field_size: usize) ![]u8 {
        var in_stream = self.deserializer.in_stream;
        var value = try self.string_arena.allocator.alloc(u8, field_size);
        std.mem.secureZero(u8, value);

        for (value) |_, i| {
            value[i] = try in_stream.readByte();
        }

        std.debug.warn("consumed string {x} {}\n", .{ value, field_size });
        return value;
    }

    fn checkValid(
        self: *Database,
        header_value: HeaderValue,
        field_size: usize,
    ) DatabaseError!void {
        switch (header_value) {
            .CipherID => |value| {
                if (!std.mem.eql(u8, value, &MAIN_CIPHER_ID)) {
                    std.debug.warn("source cipher id: '{x}' {}\n", .{
                        value,
                        value.len,
                    });
                    std.debug.warn("correct cipher id: '{x}'\n", .{MAIN_CIPHER_ID});
                    return DatabaseError.InvalidHeader;
                }
            },

            .CompressionFlags => blk: {
                var comp_flags = header_value.CompressionFlags;
                if (comp_flags == 0 or comp_flags == 1) break :blk {};
                std.debug.warn("comp_flags: {}, can only be 0 or 1\n", .{comp_flags});
                return DatabaseError.InvalidHeader;
            },

            .MasterSeed => {
                var master_len = header_value.MasterSeed.len;
                if (master_len != field_size) {
                    std.debug.warn("master seed len: {}, header len: {}\n", .{
                        master_len,
                        field_size,
                    });
                    return DatabaseError.InvalidHeader;
                }
            },

            .InnerRandomStreamID => blk: {
                var stream_id = header_value.InnerRandomStreamID;
                if (stream_id == 0 or stream_id == 1 or stream_id == 2) break :blk {};
                std.debug.warn("stream_id: {}, can only be 0, 1, 2\n", .{stream_id});
                return DatabaseError.InvalidHeader;
            },
            else => {},
        }
    }

    fn checkVersionHeader(self: *Database, version: []const u8) !void {
        if (std.mem.eql(u8, version, &VER_3_1_ID)) {
            std.debug.warn("valid 3.1 db\n", .{});
        } else if (std.mem.eql(u8, version, &VER_1_ID)) {
            std.debug.warn("INVALID DATABASE: v1\n", .{});
            return DatabaseError.InvalidVersion;
        } else if (std.mem.eql(u8, version, &VER_2_ALPHA_ID)) {
            std.debug.warn("INVALID DATABASE: v2 alpha/beta\n", .{});
            return DatabaseError.InvalidVersion;
        } else if (std.mem.eql(u8, version, &VER_2_ID)) {
            std.debug.warn("INVALID DATABASE: v2\n", .{});
            return DatabaseError.InvalidVersion;
        } else {
            std.debug.warn("unknown version: {x}\n", .{version});
            return DatabaseError.InvalidVersion;
        }
    }

    pub fn read(self: *Database) !void {
        var signature_1 = try self.deserializer.deserialize([4]u8);
        if (!std.mem.eql(u8, &signature_1, &KEEPASS_PRIMARY_ID)) {
            return DatabaseError.SignatureFail;
        }

        var version = try self.deserializer.deserialize([4]u8);
        try self.checkVersionHeader(&version);

        var minor_version = try self.deserializer.deserialize(i16);
        var major_version = try self.deserializer.deserialize(i16);
        std.debug.warn("db version: {}.{}\n", .{ major_version, minor_version });
        self.version = [_]i16{ major_version, minor_version };

        if (major_version != 3) {
            std.debug.warn("Invalid major version: {}\n", .{major_version});
            return DatabaseError.InvalidVersion;
        }

        while (true) {
            const field_id_int = try self.deserializer.deserialize(u8);
            const field_id = @intToEnum(HeaderField, field_id_int);

            const field_size_int = try self.deserializer.deserialize(i16);
            const field_size = @intCast(usize, field_size_int);
            std.debug.warn("field size: {} => {}\n", .{ field_size_int, field_size });

            var field_value: HeaderValue = switch (field_id) {
                .End => break,

                .Comment => HeaderValue{ .Comment = try self.consumeString(field_size) },
                .CipherID => HeaderValue{ .CipherID = try self.consumeString(field_size) },

                .CompressionFlags => HeaderValue{ .CompressionFlags = try self.deserializer.deserialize(i32) },

                .MasterSeed => HeaderValue{ .MasterSeed = try self.consumeString(field_size) },
                .TransformSeed => HeaderValue{ .TransformSeed = try self.consumeString(field_size) },

                .TransformRounds => HeaderValue{ .TransformRounds = try self.deserializer.deserialize(i64) },

                .EncryptionIV => HeaderValue{ .EncryptionIV = try self.consumeString(field_size) },
                .ProtectedStreamKey => HeaderValue{ .ProtectedStreamKey = try self.consumeString(field_size) },
                .StreamStartBytes => HeaderValue{ .StreamStartBytes = try self.consumeString(field_size) },

                .InnerRandomStreamID => HeaderValue{ .InnerRandomStreamID = try self.deserializer.deserialize(i32) },
            };

            switch (field_value) {
                .TransformSeed, .CipherID, .MasterSeed, .StreamStartBytes, .ProtectedStreamKey, .EncryptionIV => |value| {
                    std.debug.warn("{}: {x} {}\n", .{ field_id, value, value.len });
                },
                else => std.debug.warn("value: {x}\n", .{field_value}),
            }

            // check validity of the header value, this will error
            // if any of the header values show themselves as invalid
            try self.checkValid(field_value, field_size);
            _ = try self.dyn_header.put(field_id_int, field_value);
        }

        // skip 4 bytes. from there onwards, we have
        // our encrypted payload.
        _ = try self.deserializer.deserialize([4]u8);
    }

    pub fn sha256(self: *Database, data: []const u8) ![]const u8 {
        return try creds.sha256(self.allocator, data);
    }

    fn makeMasterKey(self: *Database, credentials: []creds.Credential) ![]const u8 {
        // there are many steps until we reach a decryption key.
        //  1: joining all credentials and hashing the result.
        //  2: transforming the key (mitigation against brute forcing)
        //  3: make master key out of sha256(masterSeed + transformedKey)
        // after all three, only then we may decrypt the file contents
        const joined_creds = try std.mem.join(self.allocator, "", credentials);
        const composite_key = try self.sha256(joined_creds);
        self.allocator.free(joined_creds);

        std.debug.warn("composite key: {x} {}\n", .{ composite_key, composite_key.len });

        var transformed_key_prehash = try trkey.transform_key(self, composite_key);
        var transformed_key = try self.sha256(transformed_key_prehash);
        std.debug.warn("transformed_key: {x} {}\n", .{ transformed_key, transformed_key.len });

        // get master seed and make the master key
        const MasterSeed = @enumToInt(HeaderField.MasterSeed);
        const master_seed = self.dyn_header.get(MasterSeed).?.MasterSeed;
        std.debug.warn("master seed: {x} {}\n", .{ master_seed, master_seed.len });

        const master_prekey = try std.mem.join(
            self.allocator,
            "",
            &[_][]const u8{ master_seed, transformed_key },
        );

        return try self.sha256(master_prekey);
    }

    /// Consume the rest of the deserializer.
    fn consumeAll(self: *Database) ![]u8 {
        var in_stream = self.deserializer.in_stream;
        return try in_stream.readAllAlloc(self.allocator, 100 * 1024);
    }

    /// Deserialize and concatenate the blocks in the file.
    /// Caller owns the returned memory.
    fn deserializeBlocks(self: *Database, deserializer: anytype) ![]u8 {
        const empty_hash = [_]u8{0} ** 32;

        var res = try self.allocator.alloc(u8, 0);

        while (true) {
            var block_id = try deserializer.deserialize(i32);
            var block_hash = try deserializer.deserialize([32]u8);
            var block_size = @intCast(usize, try deserializer.deserialize(i32));

            std.debug.warn("block {}, hash {x}, size {}\n", .{ block_id, block_hash, block_size });

            const maybe_compressed_block = try deserializeSlice(
                self.allocator,
                deserializer,
                u8,
                block_size,
            );

            if (std.mem.eql(u8, &block_hash, &empty_hash) and block_size == 0) {
                break;
            }

            // Always check hash before going into decompression

            const actual_block_hash = try self.sha256(maybe_compressed_block);
            if (!std.mem.eql(u8, actual_block_hash, &block_hash)) {
                std.debug.warn(
                    "Invalid block hash.\n\twanted: {x}\n\tactual: {x}\n",
                    .{ block_hash, actual_block_hash },
                );

                return error.BlockHashFailure;
            }

            const CompressionFlags = @enumToInt(HeaderField.CompressionFlags);
            var comp_flags = self.dyn_header.get(CompressionFlags).?.CompressionFlags;

            std.debug.warn("maybe pre-compress size: {}\n", .{maybe_compressed_block.len});

            var block_data: []const u8 = undefined;
            if (comp_flags == 1) {
                block_data = try zlib.decompressBlock(self.allocator, maybe_compressed_block);
            } else {
                block_data = maybe_compressed_block;
            }

            std.debug.warn("Reallocating output to {} + {}\n", .{ res.len, block_data.len });

            const old_position = res.len;
            const new_position = res.len + block_data.len;
            res = try self.allocator.realloc(res, new_position);

            std.mem.copy(u8, res[old_position..new_position], block_data);
        }

        return res;
    }

    /// Decrypt the database.
    pub fn decrypt(self: *Database, credentials: []creds.Credential) !void {
        const master_key = try self.makeMasterKey(credentials);
        std.debug.warn("\n\nmaster key: {x} {}\n", .{ master_key, master_key.len });

        // extract iv
        const EncryptionIV = @enumToInt(HeaderField.EncryptionIV);
        const iv = self.dyn_header.get(EncryptionIV).?.EncryptionIV;
        std.debug.warn("iv: {x} {}\n", .{ iv, iv.len });

        const StreamStartBytes = @enumToInt(HeaderField.StreamStartBytes);
        const stream_start_bytes = self.dyn_header.get(StreamStartBytes).?.StreamStartBytes;
        const stream_len = stream_start_bytes.len;
        std.debug.warn("stream start bytes: {x} {}\n", .{ stream_start_bytes, stream_start_bytes.len });

        // payload variable contains the entire file's encrypted contents
        // and, on decryption, contains our stream start bytes and then
        // the proper file.

        // by knowing at least stream start bytes we can determine if
        // our master key is successful or not.
        var payload = try self.consumeAll();
        var decrypted = try aescbc.decrypt(self, payload, iv, master_key);
        decrypted = unpad(decrypted);

        std.debug.warn(
            "decryption start bytes\n\tdecrypted\t{x}\n\twanted  \t{x}\n",
            .{ decrypted[0..stream_len], stream_start_bytes },
        );
        if (!std.mem.eql(u8, decrypted[0..stream_len], stream_start_bytes)) {
            return DatabaseError.InvalidCredentials;
        }

        var blocks = decrypted[stream_len..];
        var buf = BufferType{ .buffer = blocks, .pos = 0 };

        // create a new deserializer based on the payload that we
        // successfully decrypted.
        var block_deserial = BlockDeserializer.init(buf.inStream());
        self.decrypted_data = try self.deserializeBlocks(&block_deserial);
    }

    fn fetchStreamKey(self: *@This()) ![]const u8 {
        const protected_stream_key = self.dyn_header.get(
            @enumToInt(HeaderField.ProtectedStreamKey),
        ).?.ProtectedStreamKey;

        return try self.sha256(protected_stream_key);
    }

    fn emitSalsaStream(self: *@This(), length: usize) ![]const u8 {
        const stream_key = try self.fetchStreamKey();

        std.debug.warn("allocating salsa buffer! counter={} length={}\n", .{ self.salsa_counter, length });
        var result = try self.allocator.alloc(u8, self.salsa_counter + length);
        std.mem.secureZero(u8, result);

        std.debug.assert(stream_key.len == salsa.crypto_stream_salsa20_KEYBYTES);

        var SALSA_STREAM_IV = [_]u8{
            0xE8, 0x30, 0x09, 0x4B, 0x97, 0x20, 0x5D, 0x2A,
        };
        std.debug.assert((&SALSA_STREAM_IV).len == 8);

        //std.debug.warn("salsa20 key: {x} {}\n", .{ stream_key, stream_key.len });

        const val = salsa.crypto_stream_salsa20(result.ptr, self.salsa_counter + result.len, &SALSA_STREAM_IV, stream_key.ptr);
        //std.debug.warn("salsa20 out (should be 0): {}\n", .{val});

        //std.debug.warn("salsa20 complete stream: {x} {}\n", .{ result, result.len });
        const out = result[self.salsa_counter .. self.salsa_counter + length];
        //std.debug.warn("salsa20 wanted slice: {x} {}\n", .{ out, out.len });
        self.salsa_counter += result.len;
        return out;
    }

    fn decryptSalsaEntry(self: *@This(), encrypted_password: []const u8) ![]const u8 {
        var decrypted = try self.allocator.alloc(u8, encrypted_password.len);

        // emit N salsa20 bytes, xor them with ciphertext to get
        // plaintext password
        var stream = try self.emitSalsaStream(encrypted_password.len);
        var i: usize = 0;
        while (i < decrypted.len) : (i += 1) {
            decrypted[i] = stream[i] ^ encrypted_password[i];
        }
        std.debug.warn("ciphertext = {x} {}\n", .{ encrypted_password, encrypted_password.len });
        std.debug.warn("stream = {x} {}\n", .{ stream, stream.len });
        std.debug.warn("result (hex) {x} {}\n", .{ decrypted, decrypted.len });

        return decrypted;
    }

    pub fn ensureDecryptedEntry(self: *@This(), entry: *Entry) !void {
        if (!entry.is_protected_password) {
            return;
        }

        const stream_id = self.dyn_header.get(
            @enumToInt(HeaderField.InnerRandomStreamID),
        ).?.InnerRandomStreamID;

        const encoded_password = entry.password.?;
        var decoded_password = try self.allocator.alloc(u8, try std.base64.standard_decoder.calcSize(
            encoded_password,
        ));

        try std.base64.standard_decoder.decode(
            decoded_password,
            encoded_password,
        );

        entry.password = switch (stream_id) {
            0 => entry.password,
            1 => @panic("ARC4 streams not supported"),
            2 => try self.decryptSalsaEntry(decoded_password),
            else => @panic("unknown stream id"),
        };
    }
};
