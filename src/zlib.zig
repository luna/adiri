const std = @import("std");
const c = @cImport({
    @cInclude("archive.h");
});

// Caller owns the memory
pub fn decompressBlock(
    allocator: *std.mem.Allocator,
    input_data: []const u8,
) ![]u8 {
    var archive = c.archive_read_new();
    defer {
        _ = c.archive_read_free(archive);
    }

    std.debug.assert(c.archive_read_support_compression_gzip(archive) == c.ARCHIVE_OK);
    std.debug.assert(c.archive_read_support_format_raw(archive) == c.ARCHIVE_OK);

    switch (c.archive_read_open_memory(archive, input_data.ptr, input_data.len)) {
        c.ARCHIVE_OK => {},
        else => |val| {
            std.debug.warn("Unexpected {} from libarchive open\n", .{val});
            return error.OpenFail;
        },
    }

    var entry: ?*c.archive_entry = undefined;
    switch (c.archive_read_next_header(archive, &entry)) {
        c.ARCHIVE_OK => {},
        else => |val| {
            std.debug.warn("Unexpected {} from libarchive read entry\n", .{val});
            return error.ReadHeaderFail;
        },
    }

    // TODO: how do we discover the actual decompressed size so we can
    // allocate all in one go? maybe detect it by doing the read
    // in a while true loop and continue until we get ARCHIVE_EOF?

    var result = try allocator.alloc(u8, 0);
    errdefer allocator.free(result);

    // Decompress the data in 32KB chunks
    while (true) {
        var chunk = try allocator.alloc(u8, 32 * 1024);
        defer allocator.free(chunk);

        const size = c.archive_read_data(archive, chunk.ptr, chunk.len);
        if (size < 0) {
            std.debug.warn("Got {} from libarchive read data\n", .{size});
            return error.ReadError;
        }

        if (size == 0) break;

        const wanted_bytes = chunk[0..@intCast(usize, size)];

        const old_position = result.len;
        const new_position = result.len + wanted_bytes.len;
        result = try allocator.realloc(result, new_position);
        std.mem.copy(u8, result[old_position..new_position], wanted_bytes);
    }

    return result;
}
