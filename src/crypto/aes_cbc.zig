const std = @import("std");
const db = @import("../database.zig");
pub const c = @cImport({
    @cInclude("mbedtls/aes.h");
    @cInclude("mbedtls/error.h");
});

const Database = db.Database;

pub fn mBedPrintErr(error_number: c_int) void {
    var buf: [512]u8 = undefined;
    var error_buffer: []u8 = &buf;
    c.mbedtls_strerror(error_number, error_buffer.ptr, error_buffer.len);
    std.debug.warn("mbedtls error: {}\n", .{error_buffer});
}

fn decryptMBed(ciphertext: []u8, key: []const u8, iv: []u8, plaintext: []u8) !void {
    var ctx: c.mbedtls_aes_context = undefined;
    c.mbedtls_aes_init(&ctx);
    const setkey_res = c.mbedtls_aes_setkey_dec(
        &ctx,
        key.ptr,
        256,
    );
    if (setkey_res != 0) {
        mBedPrintErr(setkey_res);
        return error.MBedSetkeyFail;
    }

    const crypt_res = c.mbedtls_aes_crypt_cbc(
        &ctx,
        c.MBEDTLS_AES_DECRYPT,
        ciphertext.len,
        iv.ptr,
        ciphertext.ptr,
        plaintext.ptr,
    );
    if (crypt_res != 0) {
        mBedPrintErr(crypt_res);
        return error.MBedDecryptFail;
    }
}

/// Decrypts using AES-128-CBC
pub fn decrypt(
    self: *Database,
    ciphertext: []u8,
    iv: []u8,
    key: []const u8,
) ![]u8 {
    std.debug.assert(key.len == 32);
    std.debug.assert(iv.len == 16);

    var plaintext = try self.allocator.alloc(u8, ciphertext.len);
    decryptMBed(ciphertext, key, iv, plaintext) catch |err| {
        return err;
    };
    return plaintext;
}
