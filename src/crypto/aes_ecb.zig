const std = @import("std");
const db = @import("../database.zig");
const aescbc = @import("aes_cbc.zig");
const c = aescbc.c;

const Database = db.Database;

pub fn mBedPrintErr(error_number: c_int) void {
    var buf: [512]u8 = undefined;
    var error_buffer: []u8 = &buf;
    c.mbedtls_strerror(error_number, error_buffer.ptr, error_buffer.len);
    std.debug.warn("mbedtls error: {}\n", .{error_buffer});
}

pub fn encrypt(ctx: *c.struct_mbedtls_aes_context, ciphertext: []u8, plaintext: []u8) !void {
    const crypt_res = c.mbedtls_aes_crypt_ecb(
        ctx,
        c.MBEDTLS_AES_ENCRYPT,
        ciphertext.ptr,
        plaintext.ptr,
    );

    if (crypt_res != 0) {
        mBedPrintErr(crypt_res);
        return error.MBedDecryptFail;
    }
}
