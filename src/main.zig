const std = @import("std");
const os = std.os;

const database = @import("database.zig");
const Database = database.Database;

const creds = @import("credentials.zig");
const DatabaseReader = @import("reader.zig").DatabaseReader;

const File = std.fs.File;
const ArgIterator = std.process.ArgIterator;

const InternalErr = error{Stop};

const Context = struct {
    stdout: *std.fs.File,
    args: *ArgIterator,
    allocator: *std.mem.Allocator,

    pub fn nextArg(
        self: *Context,
        stream: *std.io.OutStream(File, std.os.WriteError, File.write),
        msg: []const u8,
    ) ![]const u8 {
        return try (self.args.next(self.allocator) orelse {
            try stream.print("argument missing: {}\n", .{msg});
            return InternalErr.Stop;
        });
    }
};

fn printHelp(ctx: *Context) !void {
    try ctx.stdout.outStream().print("adiri\nTODO help cmd", .{});
}

fn openDatabase(ctx: *Context) !void {
    var stream = &ctx.stdout.outStream();

    const db_path = try ctx.nextArg(stream, "no database provided");
    defer ctx.allocator.free(db_path);
    try stream.print("db path: {}\n", .{db_path});

    var file = try std.fs.cwd().openFile(db_path, .{ .read = true, .write = false });
    var file_stream = &file.inStream();

    var db = try Database.init(file_stream, ctx.allocator);
    defer db.deinit();
    try db.read();

    const credential_types = try ctx.nextArg(
        stream,
        "no credentials list provided (comma-separated list)",
    );
    defer ctx.allocator.free(credential_types);

    const credentials = try creds.fetchCredentials(credential_types, ctx.allocator, ctx.args);
    defer {
        for (credentials) |cred| {
            ctx.allocator.free(cred);
        }
        ctx.allocator.free(credentials);
    }

    for (credentials) |cred| {
        std.debug.warn("credential: {x}\n", .{cred});
    }

    try db.decrypt(credentials);

    std.debug.warn("decrypted xml ({}): {}\n", .{ db.decrypted_data.len, db.decrypted_data });

    // TODO just pass &db
    var reader = DatabaseReader.init(ctx.allocator, db.decrypted_data);
    try reader.readAll();
    var it = reader.entries.iterator();
    while (it.next()) |kv| {
        try db.ensureDecryptedEntry(kv.value);
        std.debug.warn("entry {}\n", .{kv.value});
    }
}

pub fn main() anyerror!void {
    var allocator_instance = std.heap.GeneralPurposeAllocator(.{}){};
    // defer {
    //     _ = allocator_instance.deinit();
    // }
    const allocator = &allocator_instance.allocator;

    var stdout = std.io.getStdOut();
    var args_it = std.process.args();

    // skip args[0]
    _ = args_it.skip();

    const commands = [_][]const u8{
        "help",
        "open",
    };

    const handlers = [_]fn (*Context) anyerror!void{
        printHelp,
        openDatabase,
    };

    var context = &Context{
        .stdout = &stdout,
        .args = &args_it,
        .allocator = allocator,
    };

    const command = try (args_it.next(allocator) orelse {
        try printHelp(context);
        return;
    });
    defer allocator.free(command);

    for (commands) |command_in_arr, idx| {
        if (!std.mem.startsWith(u8, command, command_in_arr)) continue;
        var handler = handlers[idx];
        handler(context) catch |err| {
            switch (err) {
                InternalErr.Stop => return,
                else => return err,
            }
        };
    }
}
