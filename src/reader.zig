const std = @import("std");
const c = @cImport({
    @cInclude("expat.h");
});

const os = std.os;

const ReaderState = enum {
    Waiting,
    FillDatabaseName,
    // We ignore <History> tags, but since <Entry> exists inside of them,
    // we need to signal the reader to not process those tags.
    InHistory,
    InEntry,
    FillEntryUUID,
    FetchEntryString,
    FetchStringKey,
    FetchStringValue,
};

pub const Entry = struct {
    id: ?[]const u8 = null,
    title: ?[]const u8 = null,
    is_protected_password: bool = false,
    password: ?[]const u8 = null,
    username: ?[]const u8 = null,
};

pub const EntryMap = std.StringHashMap(*Entry);

pub const DatabaseReader = struct {
    allocator: *std.mem.Allocator,
    raw_xml: []const u8,
    parser: c.XML_Parser = null,

    // TODO maybe check depth state so we aren't too deep into the doc
    depth: usize = 0,

    state: ReaderState = .Waiting,

    current_entry: Entry = Entry{},
    current_string_key: []const u8 = undefined,

    entries: EntryMap,
    db_name: ?[]const u8 = null,

    pub fn init(allocator: *std.mem.Allocator, raw_xml: []const u8) @This() {
        return @as(@This(), .{
            .allocator = allocator,
            .raw_xml = raw_xml,
            .entries = EntryMap.init(allocator),
        });
    }

    /// Read database and fill internal data with stuff.
    pub fn readAll(self: *@This()) !void {
        var parser = c.XML_ParserCreate("UTF-8");
        if (parser == null) @panic("failed to create parser");
        self.parser = parser;

        c.XML_SetUserData(self.parser, @ptrCast(*c_void, self));

        c.XML_SetElementHandler(
            self.parser,
            @This().startElementHandler,
            @This().stopElementHandler,
        );
        c.XML_SetCharacterDataHandler(
            self.parser,
            @This().charDataHandler,
        );

        const doc_fd = try std.os.memfd_create("adiri", 0);
        _ = try std.os.write(doc_fd, self.raw_xml);
        try std.os.lseek_SET(doc_fd, 0);

        while (true) {
            var buf: ?[*]u8 = undefined;
            var len: i32 = 0;

            buf = @ptrCast([*]u8, c.XML_GetBuffer(self.parser, 8192));
            if (buf == null) @panic("Ran out of memory for parse buffer");

            const bytes_read = @intCast(c_int, try std.os.read(doc_fd, buf.?[0..8192]));

            if (c.XML_ParseBuffer(
                self.parser,
                bytes_read,
                @as(c_int, if (bytes_read == 0) 1 else 0),
            ) == c.enum_XML_Status.XML_STATUS_ERROR) {
                const err_str = c.XML_ErrorString(c.XML_GetErrorCode(self.parser));
                std.debug.warn("Parse error at line {}:\n{}\n", .{
                    c.XML_GetCurrentLineNumber(self.parser),
                    err_str[0..std.mem.len(err_str)],
                });
                return error.ParseError;
            }

            if (bytes_read == 0)
                break;
        }
    }

    fn decodeBase64(self: *@This(), encoded: []const u8) ![]u8 {
        std.debug.warn("got encoded: {}\n", .{encoded});
        var buf = try self.allocator.alloc(u8, try std.base64.standard_decoder.calcSize(encoded));
        try std.base64.standard_decoder.decode(buf, encoded);
        return buf;
    }

    fn startElementHandler(
        userdata: ?*c_void,
        cstr_el: ?[*:0]const u8,
        carray_attributes: ?[*]?[*:0]const u8,
    ) callconv(.C) void {
        // setup to extract string
        var self = @ptrCast(*@This(), @alignCast(8, userdata.?));
        self.depth += 1;

        const el: []const u8 = std.mem.spanZ(cstr_el.?);
        const array_attributes: [*]?[*:0]const u8 = carray_attributes.?;
        //const attrs = cstr_attr[0..std.mem.len(cstr_attr)];

        std.debug.warn("tag start {}, state before {}\n", .{ el, self.state });

        // Don't care about the starting elements of the database
        if (std.mem.eql(u8, el, "KeePassFile")) return;
        if (std.mem.eql(u8, el, "Meta")) return;

        // from Waiting, we can either go to FillDatabaseName or InHistory.
        if (std.mem.eql(u8, el, "DatabaseName")) self.state = .FillDatabaseName;
        if (std.mem.eql(u8, el, "History")) self.state = .InHistory;

        // from Waiting + (no InHistory), we can go to InEntry.
        // this is because <Entry> elements may exist in <History> and we do
        // not want that.
        if (self.state != .InHistory and std.mem.eql(u8, el, "Entry")) self.state = .InEntry;

        // from InEntry we can enter two other states: FillUUID, and FetchString
        if (self.state == .InEntry and std.mem.eql(u8, el, "UUID")) self.state = .FillEntryUUID;
        if (self.state == .InEntry and std.mem.eql(u8, el, "String")) self.state = .FetchEntryString;

        // from FetchEntryString, we can get a <Key> or a <Value>.
        if (self.state == .FetchEntryString and std.mem.eql(u8, el, "Key")) self.state = .FetchStringKey;
        if (self.state == .FetchEntryString and std.mem.eql(u8, el, "Value")) {
            // iterate through attributes until we find "protected" and its result
            var i: usize = 0;
            while (array_attributes[i] != null) : (i += 2) {
                const attr_key = std.mem.spanZ(array_attributes[i].?);
                const attr_value = std.mem.spanZ(array_attributes[i + 1].?);

                std.debug.warn(
                    "current string = {}, attr key = {}, attr val = {}\n",
                    .{
                        self.current_string_key,
                        attr_key,
                        attr_value,
                    },
                );

                // check if we got a protected password entry
                if (std.mem.eql(u8, self.current_string_key, "Password") and
                    std.mem.eql(u8, attr_key, "Protected") and
                    std.mem.eql(u8, attr_value, "True"))
                {
                    std.debug.warn("got protected password\n", .{});
                    self.current_entry.is_protected_password = true;
                }
            }

            self.state = .FetchStringValue;
        }

        std.debug.warn("tag start {}, state after {}\n", .{ el, self.state });
    }

    fn stopElementHandler(userdata: ?*c_void, cstr_el: ?[*:0]const u8) callconv(.C) void {
        // setup to extract string
        var self = @ptrCast(*@This(), @alignCast(8, userdata.?));
        self.depth -= 1;

        const el: []const u8 = std.mem.spanZ(cstr_el.?);
        std.debug.warn("tag close {}, state before = {}\n", .{ el, self.state });

        // Closed <History> tag? We can handle the rest of the <Entry> tag
        if (self.state == .InHistory and std.mem.eql(u8, el, "History")) self.state = .InEntry;

        // If we got a closed <Entry> (while we are InEntry),
        // we should emit the entry we currently have into the entry map.

        // Since current_entry lives in the heap, we don't trust its lifetime,
        // instead we create an entry in the heap, copy the struct over, and
        // set the current reader's one to full-null
        if (self.state == .InEntry and std.mem.eql(u8, el, "Entry")) {
            var heap_entry = self.allocator.create(Entry) catch @panic("failed to create entry");
            heap_entry.* = Entry{
                .id = self.current_entry.id,
                .title = self.current_entry.title,
                .is_protected_password = self.current_entry.is_protected_password,
                .password = self.current_entry.password,
                .username = self.current_entry.username,
            };

            // we assume we at least have an uuid inside the Entry.
            // TODO maybe assert that all fields aren't null here,
            // convert current Entry to IncompleteEntry, and create a
            // FullEntry type without optionals.
            _ = self.entries.put(heap_entry.id.?, heap_entry) catch @panic("failed to add entry");
            std.debug.warn("ADD ENTRY {}\n", .{heap_entry});

            self.current_entry = Entry{};

            self.state = .Waiting;
        }

        std.debug.warn("tag close {}, state after = {}\n", .{ el, self.state });
    }

    // lifetime of returned string tied up to the lifetime of the input string.
    fn safeString(self: *@This(), unsafe_string: ?[*]const u8, length: c_int) []const u8 {
        return unsafe_string.?[0..@intCast(usize, length)];
    }

    fn charDataHandler(userdata: ?*c_void, unsafe_content: ?[*]const u8, len: c_int) callconv(.C) void {
        var self = @ptrCast(*@This(), @alignCast(8, userdata.?));

        switch (self.state) {
            .FillDatabaseName => {
                const db_name = self.safeString(unsafe_content, len);

                std.debug.warn("got database name: {}\n", .{db_name});
                self.db_name = db_name;
                self.state = .Waiting;
            },
            .FillEntryUUID => {
                // TODO remove panic, make parse errors instead
                const uuid = self.decodeBase64(
                    self.safeString(unsafe_content, len),
                ) catch @panic("failed to decode base64");

                std.debug.warn("got entry uuid {x}\n", .{uuid});
                self.current_entry.id = uuid;
                self.state = .InEntry;
            },
            .FetchStringKey => {
                const key_value = self.safeString(unsafe_content, len);
                std.debug.warn("Got <String> key: {}\n", .{key_value});
                self.current_string_key = key_value;
                self.state = .FetchEntryString;
            },
            .FetchStringValue => {
                const string_value = self.safeString(unsafe_content, len);

                std.debug.warn("In <String>, key={}\n", .{self.current_string_key});

                if (std.mem.eql(u8, self.current_string_key, "Title")) {
                    self.current_entry.title = string_value;
                    self.state = .InEntry;
                } else if (std.mem.eql(u8, self.current_string_key, "UserName")) {
                    self.current_entry.username = string_value;
                    self.state = .InEntry;
                } else if (std.mem.eql(u8, self.current_string_key, "Password")) {
                    // TODO decrypt XSalsa20 somehow.
                    self.current_entry.password = string_value;
                    self.state = .InEntry;
                } else {
                    // If the current <String> <Key> isn't something we want,
                    // We should just go back to searching for the next <String> tag
                    self.state = .InEntry;
                }
            },
            else => {},
        }
    }
};
