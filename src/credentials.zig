const std = @import("std");

pub const CredentialError = error{
    InvalidType,
    NoKeyfile,
    InvalidKeyfile,
};

pub const CredentialType = enum {
    Password,
    Keyfile,
};

pub const Credential = []const u8;

fn sliceToType(type_str: []const u8) !CredentialType {
    if (std.mem.eql(u8, type_str, "password")) {
        return CredentialType.Password;
    } else if (std.mem.eql(u8, type_str, "keyfile")) {
        return CredentialType.Keyfile;
    } else {
        return CredentialError.InvalidType;
    }
}

/// Caller owns returned memory.
pub fn sha256(allocator: *std.mem.Allocator, data: []const u8) ![]const u8 {
    var out = try allocator.alloc(u8, 32);
    std.crypto.Sha256.hash(data, out);
    return out;
}

/// Caller owns the returned memory.
fn getPass(allocator: *std.mem.Allocator, prompt: []const u8) ![]u8 {
    var stdin_file = std.io.getStdIn();
    var stdout_file = std.io.getStdOut();

    var char_buf: [1]u8 = undefined;
    var password = try allocator.alloc(u8, 128);
    var idx: usize = 0;

    _ = try stdout_file.write(prompt);

    while (true) {
        _ = try stdin_file.read(&char_buf);
        var char = char_buf[0];
        if (char == '\n') break;

        // TODO a way to wipe the line
        _ = try stdout_file.write("\r\r");
        _ = try stdout_file.write(prompt);
        var idx2: usize = 0;
        while (idx2 < idx) : (idx2 += 1) {
            _ = try stdout_file.write(" ");
        }

        if (idx > 128) return error.TooLong;
        password[idx] = char;
        char_buf[0] = 0;
        idx += 1;
    }

    _ = try stdout_file.write("\n");
    return password[0..idx];
}

// Caller owns returned memory.
fn fetchPassword(allocator: *std.mem.Allocator) !Credential {
    var password: []u8 = try getPass(allocator, "password: ");
    defer allocator.free(password);

    var cred = try sha256(allocator, password);
    std.debug.warn("pass '{}' ({}) => '{}' ({})\n", .{ password, password.len, cred, cred.len });
    return cred;
}

// Caller owns returned memory.
fn fetchKeyfile(allocator: *std.mem.Allocator, args: *std.process.ArgIterator) !Credential {
    const keyfile_path = try (args.next(allocator) orelse {
        return CredentialError.NoKeyfile;
    });

    var keyfile = try std.fs.cwd().openFile(keyfile_path, .{ .read = true, .write = false });
    defer keyfile.close();

    var stream = keyfile.inStream();

    // allocating max 1mb for keyfiles
    var buf = try stream.readAllAlloc(allocator, 1000 * 1024);

    return buf;
}

fn fetchSingleCredential(
    cred_type_str: []const u8,
    allocator: *std.mem.Allocator,
    args: *std.process.ArgIterator,
) !Credential {
    var cred_type = try sliceToType(cred_type_str);
    std.debug.warn("cred type: {x}\n", .{cred_type});

    var cred = switch (cred_type) {
        .Password => try fetchPassword(allocator),
        .Keyfile => try fetchKeyfile(allocator, args),
    };

    return cred;
}

// Caller owns returned memory.
pub fn fetchCredentials(
    creds_decl: []const u8,
    allocator: *std.mem.Allocator,
    args: *std.process.ArgIterator,
) ![]Credential {
    var it = std.mem.split(creds_decl, ",");
    var first_maybe = it.next();
    var credentials = std.ArrayList(Credential).init(allocator);

    if (first_maybe) |first| {
        var first_cred = try fetchSingleCredential(first, allocator, args);
        try credentials.append(first_cred);
    } else {
        // If no credentials are given, we insert a sha256() of empty string.
        var empty_pass = try sha256(allocator, "");
        try credentials.append(empty_pass);
        return credentials.items;
    }

    var idx: usize = 1;

    while (it.next()) |cred_type| {
        var cred = try fetchSingleCredential(cred_type, allocator, args);
        try credentials.append(cred);
    }

    return credentials.items;
}
